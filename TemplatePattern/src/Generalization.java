/**
 * Created by DavidDosReisInacio on 05.01.2015.
 */
public abstract class Generalization
{
    /**
     * Standardize the Skeleton of an algorithm in a "template" method
     */
    public void findSolution()
    {
        stepOne();
        stepTwo();
        stepThree();
        stepFour();
    }

    /**
     * Common implementations of individual steps are defined in base class
     */
    protected void stepOne() { System.out.println("Generalization.stepone");}

    /**
     * Steps requiring peculiar implementation are placeholders in the base class
     */
    abstract protected void stepTwo();
    abstract protected void stepThree();

    protected void stepFour() { System.out.println("Generalization.stepFour");}

}
