/**
 * Created by DavidDosReisInacio on 05.01.2015.
 * Vorteile des Template-Patterns:
 *      - Struktur des Algorithmus können in oberster Basisklasse definiert werden.
 *      - Anschliessende spezifische Implementationen werden in Sub-Klassen implementiert (saubere Trennung)
 *      - Standard-Implementationen in den Basisklassen
 *      - Sub-Klassen können Methoden der Basisklassen überschreiben (weitere spezifizierung)
 */
public class TemplatePatternDemo
{
    public static void main(String[] args) {
        Generalization algorithm = new Realization();
        algorithm.findSolution();
    }
}
