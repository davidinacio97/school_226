/**
 * Created by DavidDosReisInacio on 05.01.2015.
 */
public abstract class Specialization extends Generalization {

    @Override
    protected void stepThree() {
        step3_1();
        step3_2();
        step3_3();
    }

    /**
     * Common implementations of individual steps are defined in base class
     */
    protected void step3_1() { System.out.println("Specialization.step3_1");}

    /**
     * Steps requiring special implementation are "placeholders" in the base class
     */
    abstract protected void step3_2();
    protected void step3_3() { System.out.println("Specialization.step3_3");}
}
