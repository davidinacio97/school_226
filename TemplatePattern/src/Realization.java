/**
 * Created by DavidDosReisInacio on 05.01.2015.
 */
public class Realization extends Specialization
{

    /**
     * Steps requiring special implementation are "placeholders" in the base class
     */
    @Override
    protected void step3_2() {
     System.out.println("Specialization.step3_2");
    }

    /**
     * Steps requiring peculiar implementation are placeholders in the base class
     */
    @Override
    protected void stepTwo()
    {
        System.out.println("Realization.stepTwo");
    }

    @Override
    protected void stepFour() {
        System.out.println("Realization.stepFour");
        super.stepFour();
    }
}
