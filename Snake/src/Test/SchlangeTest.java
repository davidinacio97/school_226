package Test;

import org.junit.Test;
import snake1.Diamant;
import snake1.Game;
import snake1.Schlange;
import snake1.Spielgrenze;
import snake2.Direction;

import java.awt.*;

import static org.junit.Assert.*;

public class SchlangeTest {

    @Test
    public void testAddBodyPart() throws Exception {
        Schlange s = new Schlange(new Point(50,50), Direction.Right);
        assertEquals("Die Schlange muss bei der Instanziierung genau 1 Körperteil besitzen.", 1, s.countBodyParts());
        s.addBodyPart();
        assertEquals("Die Schlange ist nicht gewachsen.", 2, s.countBodyParts());
    }

    @Test
    public void testMove() throws Exception {
        Schlange s = new Schlange(new Point(50,50), Direction.Right);
        // Teste Startkoordinaten.
        assertEquals("X-Position nicht korrekt bei Instanziierung.", 50, s.get_positions().x);
        assertEquals("Y-Position nicht korrekt bei Instanziierung.", 50, s.get_positions().y);
        // Bewege Schlange
        s.move();

        assertEquals("Die Schlange hat sich nicht korrekt nach Rechts bewegt.", 50 + Game.UNIT, s.get_positions().x);

        s.set_bewegungsRichtung(Direction.Down);

        s.move();

        assertEquals("Die Schlange hat sich nicht korrekt nach Unten bewegt.", 50 + Game.UNIT, s.get_positions().y);

        s.set_bewegungsRichtung(Direction.Left);
        s.move();

        assertEquals("Die Schlange hat sich auf der Y-Achse nicht korrekt nach Links bewegt.", 50 + Game.UNIT, s.get_positions().y);
        assertEquals("Die Schlange hat sich auf der X-Achse nicht korrekt nach Links bewegt.", 50, s.get_positions().x);

    }

    @Test
    public void testCheckCollision() throws Exception {

        // Testschlangen
        Schlange s = new Schlange(new Point(50,50), Direction.Right);
        Schlange s2 = new Schlange(new Point(520, 20), Direction.Right);

        // Testdiamanten
        Diamant d1 = new Diamant(new Point(50, 50));
        Diamant d2 = new Diamant(new Point(49, 50));
        Diamant d3 = new Diamant(new Point(50, 100));

        // Testspielgrenzen
        Spielgrenze spielgrenze = new Spielgrenze(new Point(20,20), 500, 500);

        // Kollision mit Diamanten testen
        assertEquals("Die Schlange muss mit dem Diamanten kollidieren.", true, d1.checkCollision(s));
        assertEquals("Die Schlange muss mit dem Diamanten kollidieren.", true, d2.checkCollision(s));
        assertEquals("Die Schlange darf nicht mit dem Diamanten kollidieren.", false, d3.checkCollision(s));

        // Kollision mit Spielfeldrand testen
        assertEquals("Die Schlange darf nicht mit dem Spielrand kollidieren.", false, spielgrenze.checkCollision(s));
        assertEquals("die Schlange muss mit dem Spielrand kollidieren.", true, spielgrenze.checkCollision(s2));

    }

    /**
     * Testet ob die Schlange stirbt.
     * @throws Exception
     */
    @Test

    public void testDie() throws Exception {
        Schlange s = new Schlange(new Point(50,50), Direction.Right);
        assertEquals("Die Schlange muss bei der Instanziierung leben.",true, s.isAlive());
        s.die();
        assertEquals("Die Schlange muss Tod sein.", false, s.isAlive());
    }

    @Test
    public void testSet_bewegungsRichtung() throws Exception {
        Schlange s = new Schlange(new Point(50, 50), Direction.Right);
        assertEquals("Die Schlange muss die Startrichtung Rechts haben.", Direction.Right, s.get_bewegungsRichtung());
        s.set_bewegungsRichtung(Direction.Down);
        assertEquals("Die Schlange muss die Richtung Down haben.", Direction.Down, s.get_bewegungsRichtung());
    }
    
    @Test
    public void testGet_bewegungsRichtung() throws Exception
    {
    	Schlange s = new Schlange(new Point(50,50), Direction.Right);
    	assertEquals("Die Schlange muss die Richtung rechts haben.", Direction.Right, s.get_bewegungsRichtung());
    	s.set_bewegungsRichtung(Direction.Down);
    	assertEquals("Die Schlange muss die Richtung Down haben.", Direction.Down, s.get_bewegungsRichtung());
    	
    }
    
    @Test
    public void testIsAlive() throws Exception
    {
    	Schlange s = new Schlange(new Point(50,50), Direction.Right);
    	assertEquals("Die Schlange muss bei der Instanziierung leben.", true, s.isAlive());
    	s.die();
    	assertEquals("Die Schlange muss Tod sein.", false, s.isAlive());
    }
    
    @Test
    public void testcountBodyParts() throws Exception
    {
    	Schlange s = new Schlange(new Point(50, 50), Direction.Right);
    	assertEquals("Die Schlange darf nur 1 K�rperteil besitzen.", 1, s.countBodyParts());
    	s.addBodyPart();
    	s.addBodyPart();
    	s.addBodyPart();
    	assertEquals("Die Schlange muss 4 K�rperteile besitzen", 4, s.countBodyParts());
    	
    }   
    
}