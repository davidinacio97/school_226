package Test;

import org.junit.Test;
import snake1.Diamant;
import snake1.Schlange;
import snake2.Direction;

import java.awt.*;

import static org.junit.Assert.*;

public class DiamantTest {

    @Test
    /**
     * Testet ob eine Schlange mit einem Diamanten korrekt kolidiert.
     */
    public void testCheckCollision() throws Exception {
        // Schlange f�r Kollision
        Schlange s1 = new Schlange(new Point(50,50), Direction.Right);
        // Diamant f�r Kollision, gleiche Koordinaten wie Schlange.
        Diamant d1 = new Diamant(new Point(50,50));
        // Diamant f�rr Kollision, 1 Punkt weiter nach Links als Schlange.
        Diamant d2 = new Diamant(new Point(49,50));
        // Diamant f�r Kollision, grenzt an Schlange.
        Diamant d3 = new Diamant(new Point(50, 100));

        assertEquals("Die Schlange muss mit dem Diamanten kollidieren.", true, d1.checkCollision(s1));
        assertEquals("Die Schlange muss mit dem Diamanten kollidieren.", true, d2.checkCollision(s1));
        assertEquals("Die Schlange daf nicht mit dem Diamanten kollidieren.", false, d3.checkCollision(s1));
    }
}