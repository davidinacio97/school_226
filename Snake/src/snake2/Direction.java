package snake2;

/**
 *  Beschreibt die Richtung der Schlange.
 */
public enum Direction {
	Right,
	Left,
	Up,
	Down
}
