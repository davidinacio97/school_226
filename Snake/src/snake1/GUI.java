package snake1;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import snake2.Direction;


/** 
 *  Die Klasse GUI stellt das Fenster (JFrame) und 
 *  das Spielfeld (JPanel) des Snake-Spiels zur Verf�gung.
 *  
 *  @author A. Scheidegger, M.Frieden
 *  @version 4.0
 */

@SuppressWarnings("serial")
public class GUI extends JFrame implements KeyListener{

  /** Referenz auf das Game-Objekt. */
  private Game game;

  /** Das JPanel, welches als Spielfeld dient. Anonyme innere Klasse. */
  private JPanel spielfeld = new JPanel() {
    /**
     * Wird automatisch aufgerufen, wenn das Fenster neu gezeichnet werden
     * muss. Delegiert das Zeichnen des Spiels an das Game-Objekt.
     * @param g Graphics-Objekt zum Zeichnen
     */
    @Override
    public void paintComponent(Graphics g){
      game.draw(g);
    }
  };

  /**
   * Konstruktor. Initialisiert den Frame. Registriert das Game-Objekt
   * als KeyListener.
   */
  public GUI(Game game) {
    this.game= game;
    setSize(600, 400);
    setTitle("Snake V1.0");
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setContentPane(spielfeld);
    setVisible(true);
    this.addKeyListener(this);
    this.setFocusable(true);
  }

  /**
   * Gibt die Breite des Spielfeldes zur�ck.
   * @return Breite des Spielfeldes.
   */
  public int getBreite() {
    return spielfeld.getWidth();
  }

  /**
   * Gibt die H�he des Spielfeldes zur�ck.
   * @return H�he des Spielfeldes
   */
  public int getHoehe()
  {
    return spielfeld.getHeight();
  }

@Override
public void keyPressed(KeyEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void keyReleased(KeyEvent e) 
{
	if (e.getKeyCode() == KeyEvent.VK_RIGHT)
	{
		game.set_direction(Direction.Right);
	}
	else if (e.getKeyCode() == KeyEvent.VK_LEFT)
	{
		game.set_direction(Direction.Left);
	}
	else if (e.getKeyCode() == KeyEvent.VK_UP)
	{
		game.set_direction(Direction.Up);
	}	
	else if (e.getKeyCode() == KeyEvent.VK_DOWN)
	{
		game.set_direction(Direction.Down);
	}	
}

@Override
public void keyTyped(KeyEvent arg0) {
	// TODO Auto-generated method stub
	
}
}
