package snake1;
import java.awt.*;
import java.util.*;
import snake2.Direction;

public class Game {

    /**
     * Vector mit Spielelement-Objekten.
     */
    private Vector<Spielelement> _playElements = new Vector<Spielelement> ();
    
    /**
     * Speichert die Richtung der Schlange.
     */
    private Direction _direction = Direction.Right;
        
    /**
     * Konstante für die Anzahl Pixel welche sich bewegt werden.
     */
    public static final int UNIT = 20;
    
    /**
     * Speichert die Länge des Spielfeldes.
     */
    private int _fieldLength = 0;
    
    /**
     * Speichert die Breite des Spielfeldes
     */
    private int _fieldWidth = 0;

	/**
	 * Speichert die Anzahl Punkte des Spielers.
	 */
	private int _points = 0;


	/**
     * @param g Graphics-Objekt
     * Zeichnet die Spielelement-Objekte auf das Spielfeld.
     */
    public void draw(Graphics g)
    {
    	for (Spielelement element : _playElements)
    	{
    		element.draw(g);
    	}
    }

	/**	 *
	 * @return Anzahl erreichte Punkte.
	 */
	public int getPoints()
	{
		return _points;
	}

	/**
	 * Gibt die Liste mit Spielelementen zurück.
	 * @return Liste mit Spielelementen.
	 */
	public Vector<Spielelement> get_playElements()
	{
		Vector<Spielelement> copyPlayElementes = new Vector<Spielelement>(_playElements);

		return copyPlayElementes;

	}
    
    /**
	 * @return the _direction
	 */
	public Direction get_direction() {
		return _direction;
	}

	/**
	 * @param _direction the _direction to set
	 */
	public void set_direction(Direction _direction) {
		this._direction = _direction;
	}
	
	/**
	 * @param snake Schlange
	 * Überprüft ob die Schlange mit einem Spielelement kollidiert ist.
	 */
	public boolean checkCollision(Schlange snake)
	{
		for (Spielelement element : _playElements)
		{
			if (element.checkCollision(snake))
			{
				if (element.getClass() == Diamant.class)
				{
					_points += ((Diamant)(element)).get_Value();
					this.removeAllDiamons();
					this.addDiamond();
					snake.addBodyPart();
					return false;
				}
				else
				{
					snake.die();
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Löscht alle Diamant-Objekte aus der _playElements-Liste.
	 */
	private void removeAllDiamons()
	{
		for (Spielelement element : _playElements)
		{
			if (element.getClass() == Diamant.class)
			{
				_playElements.remove(element);
				break;
			}
			
		}
	}

	/**
	 * Erstellt einen neuen Diamanten auf dem Feld.
	 */
	public void addDiamond()
	{
		int x = Zufallsgenerator.zufallszahl(20, this.get_fieldWidth() - 40);
		int y = Zufallsgenerator.zufallszahl(20, this.get_fieldLength() - 40);
		_playElements.add(new Diamant(new Point(x, y)));
	}
	
	/**
     * Standardkonstruktor
     */
    public Game()
    {   	
    }
    
    /**
	 * @return the _fieldLength
	 */
	public int get_fieldLength() {
		return _fieldLength;
	}

	/**
	 * @param _fieldLength the _fieldLength to set
	 */
	public void set_fieldLength(int _fieldLength) {
		this._fieldLength = _fieldLength;
	}

	/**
	 * @return the _fieldWidth
	 */
	public int get_fieldWidth() {
		return _fieldWidth;
	}

	/**
	 * @param _fieldWidth the _fieldWidth to set
	 */
	public void set_fieldWidth(int _fieldWidth) {
		this._fieldWidth = _fieldWidth;
	}

	/**
     * @param args Startparameter
     * Einstiegsmethode ins Spiel.
     */
    public static void main(String [] args)
    {
    	Game game = new Game();
    	GUI gui = new GUI(game);
    	
    	game.set_fieldLength(gui.getHoehe() - 40);
    	game.set_fieldWidth(gui.getBreite() - 40);

    	// Zeichne Spielfeldgrenze
    	game._playElements.add(new Spielgrenze(new Point(20, 20), gui.getBreite() - 40, gui.getHoehe() - 40));
    	
    	// Zeichne ersten Diamanten.
    	game.addDiamond();
    	
    	// Zeichne Schlange
    	 Schlange schlange = new Schlange(new Point(100, 100), Direction.Right);
    	 game._playElements.add(schlange);
		schlange.addBodyPart();
		for (int i = 0; i < 5; i++) {
			schlange.addBodyPart();

		}

    	 while (schlange.isAlive())
    	 {
    		 try 
    		 {
    			 Thread.sleep(300);
    		 }
    		 catch (InterruptedException e)
    		 {
    			 
    		 }
			 // Setze Richtung der Schlange.
			 schlange.set_bewegungsRichtung(game.get_direction());

    		 if (game.checkCollision(schlange))
    		 {
    			 break;
    		 }
			 gui.setTitle("Snake Punkte: " + game.getPoints());
    		 schlange.move();
    		 gui.repaint();
    	 }

		gui.setTitle("------------- GAME OVER -------------");
    	
    }

}
