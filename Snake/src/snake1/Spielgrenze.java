package snake1;

import java.awt.*;

import snake2.Direction;

/**
 * @project Snake_Iteration1
 * @author David Dos Reis Inacio
 * Spielgrenze Klasse, erbt von Spielelement.
 * Zeichnet die Spielgrenzen des Spielfeldes.
 */
public class Spielgrenze extends Spielelement{
	
    /**
     * Speichert die Breite des Spielfeldes.
     */
    private int _width;
    
    /**
     * Speichert die H�he des Spielfeldes.
     */
    private int _height;

    /**
     * @param positions Startposition der Spielgrenzen.
     * @param width Breite des Spielfeldes.
     * @param height Hoehe des Spielfeldes.
     * Konstruktor mit Paramter.
     */
    public Spielgrenze(Point positions, int width, int height)
    {
    	super(positions);
    	_width = width;
    	_height = height;
    	
    }
    
    /* (non-Javadoc)
     * @see snake1.Spielelement#draw(java.awt.Graphics)
     */
    public void draw(Graphics g) 
    {
    	// Zeichne obere Linie.
    	g.setColor(Color.DARK_GRAY);
    	g.fillRect(_positions.x, _positions.y, _width, _height);    	
    	g.setColor(Color.BLACK);
    }

    /**
     * @param s Schlangen-Objekt
     * @return Ob Schlange kollidiert ist.
     * �berpr�ft ob die Schlange kollidiert ist.
     */
    public boolean checkCollision(Schlange s) 
    {
    	return !(this.getCollisionRectangle().contains(s.getCollisionRectangle()));
    }

	@Override
	public Rectangle getCollisionRectangle() 
	{
		return new Rectangle(_positions.x, _positions.y, _width, _height);		
	}

}
