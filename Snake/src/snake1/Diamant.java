package snake1;

import java.awt.*;

/**
 * @project Snake_Iteration1
 * @author David Dos Reis Inacio
 * Diamant-Klasse, erbt von Spielelement.
 * Zeichnet einen neuen Diamanten.
 * Repo test Comment
 */
public class Diamant extends Spielelement {
	
    /**
     * Speichert den Wert des Diamanten.
     */
    private int _value;
       
    
    /**
     * Speichert die Breite des Diamanten. 
     */
    private static int _width = 20;
    
    
    /**
     * Speichert die H�he des Diamanten.
     */
    private static int _height = 20;

    /**
     * Diamant-Konstruktor mit Parameter.
     * @param positions Startposition des Diamanten auf dem Spielfeld.
     */
    public Diamant(Point positions) {
    	super(positions);
    	_value = Diamant.getValue();
    }

    /* (non-Javadoc)
     * @see snake1.Spielelement#draw(java.awt.Graphics)
     */
    public void draw(Graphics g) 
    {
    	g.setColor(Diamant.getColor(this._value));
    	g.fillOval(_positions.x, _positions.y, _width, _height);
    	g.setColor(Color.BLACK);
    }

    /**
     * @param s Schlangen-Objekt
     * @return Ob Schlange kollidiert ist.
     * Überprüft ob die Schlange kollidiert ist.
     */
    public boolean checkCollision(Schlange s) {
        return this.getCollisionRectangle().intersects(s.getCollisionRectangle());
    }

    /**
     * @return Gibt den Wert des Diamanten zurück.
     */
    public int get_Value()
    {
    	return _value;
    }

	@Override
    /**
     * Gibt ein Kollisionsrechteck-Objekt zurück.
     */
	public Rectangle getCollisionRectangle() {
		return new Rectangle(_positions.x, _positions.y, _width, _height);
	}

    /**
     * Generiert den Wert des Diamanten.
     * @return Wert des Diamanten.
     */
    private static int getValue()
    {
        return Zufallsgenerator.zufallszahl(1,5);
    }

    /**
     * Gibt die entsprechende Farbe für den Wert des Diamanten zurück.
     * @param value
     * @return
     */
    private static Color getColor(int value)
    {
        switch(value)
        {
            case 1:
                return Color.black;
            case 2:
                return Color.lightGray;
            case 3:
                return Color.MAGENTA;
            case 4:
                return Color.cyan;
            case 5:
                return Color.WHITE;
        }
        return Color.black;
    }
}
