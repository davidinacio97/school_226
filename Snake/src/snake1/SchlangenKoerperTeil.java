package snake1;
import java.awt.*;

import snake2.Direction;

/**
 * @project Snake_Iteration1
 * @author David Dos Reis Inacio
 * SchlangenKoerperTeil-Klasse, ist ein Teil des Schlangenk�rpers.
 *
 */
public class SchlangenKoerperTeil {
	
    /**
     * Speichert die Startpositon.
     */
    private Point _positions;
        
    /**
     * Speichert die H�he des K�rperteils.
     */
    public static int Height = 20;
        
    /**
     * Speichert die Breite des K�rperteils.
     */
    public static int Width = 20;
    
    /**
     * Speichert die Bewegungsrichtung des K�rperteils.
     */
    private Direction _direction = Direction.Right;
    
    /**
     * Konstruktor mit Parameter.
     * @param positions Startpunkt der Figur.
     * @param direction Bewegungsrichtung.
     */
    public SchlangenKoerperTeil(Point positions, Direction direction) 
    {    	
    	_positions = positions;
    	_direction = direction;
    }

    /**
     * @param g Graphics-Objekt.
     */
    public void draw(Graphics g) 
    {    	
    	g. fillRect(_positions.x, _positions.y, Width, Height);	    	
    }
    
    /**
     * @return Gibt den Startpunkt des nächsten SchlangenKoerperTeils zurück.
     */
    public Point getNextStartPoint()
    {
    	switch (_direction)
    	{
    	case Right:
    		return new Point(_positions.x - Width, _positions.y);
    	case Left:
    		return new Point(_positions.x + Width, _positions.y);
    	case Down:
    		return new Point(_positions.x, _positions.y - Height);
    	case Up:
    		return new Point(_positions.x, _positions.y + Height);		
    	}
		return null;
    }
    
 /**
 * Bewegt das Schlangenk�rperteil
 */
public void move()
    {
    	switch(_direction)
    	{
    	case Up:
    		_positions.y -= Game.UNIT;
    		break;
    	case Down:
    		_positions.y += Game.UNIT;
    		break;
    	case Right:
    		_positions.x += Game.UNIT;
    		break;
    	case Left:
    		_positions.x -= Game.UNIT;  		
    		break;
    	}
    }
    
    /**
     * Gibt die Koordinaten des Schlangenkörperteils zurück.
     * @return Positionen
     */
    public Point GetPosition()
    {
    	return _positions;
    }
    
    /**
     * Setzt die Richtung des K�rperteils.
     * @param direction Richtung des K�rperteils.
     */
    public void SetDirection(Direction direction)
    {
    	_direction = direction;
    }
    
    /**
     * @return Gibt die Richtung zur�ck.
     */
    public Direction getDirection()
    {
    	return _direction;
    }
    
    /**
     * Gibt ein Kollisionsrechteck zur�ck.
     * @return Kollisionsrechteck
     */
    public Rectangle collisionRectangle()
    {
    	return new Rectangle(_positions.x, _positions.y, Width, Height);
    }
}
