package snake1;

import snake2.*;
import java.awt.*;
import java.util.*;

/**
 * @project Snake_Iteration1
 * @author David Dos Reis Inacio
 * Schlange-Klasse, erbt von Spielelement.
 * Zeichnet eine Schlange auf das Spielfeld.
 *
 */
public class Schlange extends Spielelement{
	
    /**
     * Speichert ob die Schlange noch lebt.
     */
    private boolean _isAlive = true;
    
    /**
     * Speichert die Richtung der Schlange.
     */
    private Direction _bewegungsRichtung = Direction.Right;

    /**
     * Speichert die Körperteile der Schlange.
     */
    private Vector<SchlangenKoerperTeil> _bodyParts;

    /**
     * Schlange-Konstruktor mit Parameter.
     * @param positions Startposition der Schlange.
     */
    public Schlange(Point positions, Direction direction)
    {
    	super(positions);
    	_bodyParts = new Vector<SchlangenKoerperTeil>();
    	_bodyParts.add(new SchlangenKoerperTeil(positions, _bewegungsRichtung));
    	set_bewegungsRichtung(direction);
    }
    
    /**
     * Fügt ein Körperteil der Schlange hinzu.
     */
    public void addBodyPart()
    {
    	_bodyParts.add(new SchlangenKoerperTeil(_bodyParts.get(_bodyParts.size() - 1).getNextStartPoint(), _bodyParts.get(_bodyParts.size() - 1).getDirection()));
    }

    /**
     * Verschiebt die Schlange.
     */
    public void move() 
    {
    	this.set_BodyPart_Direction();

		for (SchlangenKoerperTeil bodyPart : _bodyParts)
    	{
    		bodyPart.move();
    	}
    }

    /* (non-Javadoc)
     * @see snake1.Spielelement#draw(java.awt.Graphics)
     */
    public void draw(Graphics g) 
    {
    	for (SchlangenKoerperTeil bodyPart : _bodyParts)
    	{
    		bodyPart.draw(g);
    	}
    }

	/**
	 * @param s Schlangen-Objekt
	 * @return Ob Schlange kollidiert ist.
	 * Überprüft ob die Schlange kollidiert ist.
	 */
	@Override
	public boolean checkCollision(Schlange s)
	{
		switch(this._bodyParts.get(0).getDirection())
		{
			case  Down:
				if(_bodyParts.get(1).getDirection() == Direction.Up) {return true; } break;
			case Up:
				if(_bodyParts.get(1).getDirection() == Direction.Down){ return true; } break;
			case Right:
				if(_bodyParts.get(1).getDirection() == Direction.Left) {return true;} break;
			case Left:
				if(_bodyParts.get(1).getDirection() == Direction.Right) {return true;} break;

		}

		for (int i = 1; i < _bodyParts.size() - 1; i++)
		{
			if (this.getCollisionRectangle().intersects(_bodyParts.get(i).collisionRectangle()))
			{
				return true;
			}
		}
		return false;
	}

	/**
     * @return Gibt zurück ob die Schlange lebt oder nicht.
     */
    public boolean isAlive()
    {
    	return _isAlive;
    }
    
    /**
     * Lässt die Schlange sterben.
     */
    public void die()
    {
    	_isAlive = false;
    }

	/**
	 * @param _bewegungsRichtung the _bewegungsRichtung to set
	 */
	public void set_bewegungsRichtung(Direction _bewegungsRichtung) {
		this._bewegungsRichtung = _bewegungsRichtung;
	}

	/**
	 * Gibt die Richtung zurück.
	 * @return Bewegungsrichtung.
	 */
	public Direction get_bewegungsRichtung() {return this._bewegungsRichtung;}

	/**
	 * Gibt die Anzahl Körperteile der Schlange zurück.
	 * @return Anzahl Körperteile
	 */
	public int countBodyParts() { return _bodyParts.size();}

	/**
	 * Setzt die Richtung der Schlangenteile.
	 */
	private void set_BodyPart_Direction()
	{

		for (int i = _bodyParts.size() - 1; i > 0; i--)
		{
			_bodyParts.get(i).SetDirection(_bodyParts.get(i - 1).getDirection());
		}
		
		_bodyParts.get(0).SetDirection(_bewegungsRichtung);
	}
	
	/* (non-Javadoc)
	 * @see snake1.Spielelement#getCollisionRectangle()
	 */
	public Rectangle getCollisionRectangle()
	{
		return  _bodyParts.get(0).collisionRectangle();
	}
}
