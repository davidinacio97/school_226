package snake1;
import java.awt.*;


/**
 * @project Snake_Iteration1
 * @author David Dos Reis Inacio
 * Basisklasse Spielelement, zeichnet ein Element auf das Spielfeld.
 */
public abstract class Spielelement {
	
    /**
     * Startpositionen des Elements.
     */
    protected Point _positions;

    /**
     * @param positions Startpositionen
     */
    public Spielelement(Point positions) {
    	_positions = positions;
    }

    /**
     * Gibt die Position des Spielelementes zurück.
     * @return Position des Spielelementes.
     */
    public Point get_positions() {return _positions;}

    /**
     * @param g Graphics-Objekt.
     * Zeichnet das Spielelement auf das Feld.
     */
    public abstract void draw(Graphics g);

    /**
     *
     * @param s Schlangen-Objekt
     * @return Ob Schlange kollidiert ist.
     * Überprüft ob die Schlange kollidiert ist.
     */
    public abstract boolean checkCollision(Schlange s);
    
    /**
     * @return Gibt ein Kollisionsrechteck zurück.
     * Gibt ein Rechteck zurück, welches die Kollisionspunkte repräsentiert.
     */
    public abstract Rectangle getCollisionRectangle();

}
