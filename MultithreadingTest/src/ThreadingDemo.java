
public class ThreadingDemo extends Thread{
	
	private String myName;
	
	public ThreadingDemo(String name)
	{
		myName = name;
	}
	
	public void run()
	{
		for (int i = 1; i <= 100000; i++)
		{
			System.out.println(myName + " (" + i + ")");
		}
	}

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		ThreadingDemo t1 = new ThreadingDemo("Thread 1");
		ThreadingDemo t2 = new ThreadingDemo("Thread 2");
		
		t1.start();
		t2.start();
	}

}
