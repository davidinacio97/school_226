/**
 * Interface, welches ein Iterator Objekt zurückgibt.
 */
public interface Container
{
    /**
     * Gibt ein Iterator-Objekt zurück.
     * @return Iterator-Objekt
     */
    public Iterator getIterator();
}
