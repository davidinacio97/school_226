import java.util.Vector;

/**
 * NameRepository implementiert das Interface Container.
 */
public class NameRepository implements Container
{
    // Vektor mit Namen
    public Vector<String> names = new Vector<String>();

    public NameRepository()
    {
        names.add("Robert");
        names.add("John");
        names.add("Julie");
        names.add("Lora");
    }

    /**
     * Gibt ein Iterator-Objekt zurück.
     * @return Iterator-Objekt
     */
    @Override
    public Iterator getIterator() {
        return new NameIterator();
    }

    /**
     * NameIterator implementiert das Interface Iterator.
     * Iteriert durch den names-Vektor.
     */
    private class NameIterator implements Iterator
    {
        int index;

        /**
         * Gibt zurück ob das Array eine weitere Position hat.
         * @return Boolean, ob der Zeiger an der letzen Position ist.
         */
        @Override
        public boolean hasNext()
        {
            if(index < names.size())
            {
               return true;
            }
            return false;
        }

        /**
         * Gibt das Objekt zurück, welches an der Position des Zeigers ist.
         * @return Objekt
         */
        @Override
        public Object next() {
            if(this.hasNext())
            {
                return names.get(index++);
            }
            return null;
        }
    }
}