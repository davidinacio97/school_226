/**
 * Created by DavidDosReisInacio on 05.01.2015.
 * Vorteile dieses Iterator-Patterns:
 *      - Daten werden sauber via Iterator-Objekt geholt.
 *      - Keine IndexOutOfRange Exceptions durch interne Methode hasNext()
 */
public class IteratorPatternDemo
{
    public static void main(String[] args) {
        /**
         * Instanziiere NameRepository
         */
        NameRepository nameRepo = new NameRepository();


        for(Iterator iter = nameRepo.getIterator(); iter.hasNext();)
        {
            String name = (String)iter.next();
            System.out.println("Name: " + name);
        }
    }
}
