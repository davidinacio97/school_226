/**
 * Iterator-Interace, navigiert durch das Array.
 */
public interface Iterator
{
    /**
     * Gibt zurück ob das Array eine weitere Position hat.
     * @return Boolean, ob der Zeiger an der letzen Position ist.
     */
    public boolean hasNext();

    /**
     * Gibt das Objekt zurück, welches an der Position des Zeigers ist.
     * @return Objekt
     */
    public Object next();
}