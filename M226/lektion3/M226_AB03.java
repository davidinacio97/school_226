
package lektion3;

import java.util.Arrays;
import java.util.List;

import csvHandling.*;


public class M226_AB03 {

	public static void main(String[] args) 
	{
		//double[] grades = createArray();
		//calculateMaxGrade(grades);
		//calculateMedium(grades);
		//calculateMinGrade(grades);
		//calculatePI();
		
		// Write Array to Console
		//printArray(getPiDifference(calculatePI()));		
		
		//Write Array to File in C:
		/*csvHandling.writeCsvFlie(getPiDifference(calculatePI()), "c:\\test.csv", ";");
		
		if (checkRead() == false)
		{
			System.out.println("read Csv funktioniert nicht");
		}*/
		
		
	}
		
	public static double[] createArray()
	{
		double[] schulnoten = new double[11];
		
		double tempNote = 3.5;
		
		for (int i = 0; i < schulnoten.length; i++)
		{
			schulnoten[i] = tempNote;
			tempNote += 0.25;
		}
		
		return schulnoten;
	}
	
	public static void calculateMedium(double[] schoolgrades)
	{
		// Berechnung Durchschnitt
		
		double medium = 0;
		
		for (int i = 0; i < schoolgrades.length; i++)
		{
			medium += schoolgrades[i];
		}
		
		medium /= schoolgrades.length;
		
		System.out.println("Der Durchschnitt der Noten betr�gt: " + medium);
	}
	
	public static void calculateMaxGrade(double [] schoolgrades)
	{
		double maxGrade = 0;
		
		for (int i = 0; i < schoolgrades.length; i++) 
		{
			if (schoolgrades[i] > maxGrade)
			{
				maxGrade = schoolgrades[i];
			}
		}
		
		System.out.println("Die h�chste Note ist: " + maxGrade);
	}
	
	public static void calculateMinGrade(double[] schoolgrades)
	{
		double minGrade = 6.0;
		
		for (int i = 0; i < schoolgrades.length; i++)
		{
			if (schoolgrades[i] < minGrade)
			{
				minGrade = schoolgrades[i];
			}
		}
		
		System.out.println("Die kleinste Note ist: " + minGrade);
	}

	public static double[] calculatePI()
	{
		int picounter = 3;
		double[] piResult = new double[1000000];
		boolean addition = false;
		
		for (int i = 0; i < piResult.length; i++)
		{
			if (addition == true)
			{
				if (i - 1 != -1)
				{
					piResult[i] = ((piResult[i - 1]) + (1.0/picounter));
				}
				else
				{
					piResult[i] = 1.0;
					continue;
				}
			}
			else 
			{
				if (i - 1 != -1)
				{
					piResult[i] = ((piResult[i - 1]) - (1.0/picounter));
				}
				else 
				{
					piResult[i] = 1.0;
					continue;
				}
			}
			picounter += 2;
			addition = !addition;
		}
		
		double[] orderedArray = Arrays.copyOfRange(piResult, 1, piResult.length);

		return orderedArray;
		
	}
	
	public static double[][] getPiDifference(double[] valuesToCompare)
	{
		double[][] differences = new double[valuesToCompare.length][2];

		for (int i = 0; i < valuesToCompare.length; i++)
		{
			differences[i][0] = valuesToCompare[i];
			differences[i][1] = (valuesToCompare[i] *4) - Math.PI;
		}
		return differences;
	}
	
	// Gibt ein Array komplett aus
	public static void printArray(List<?> values)
	{
		for (Object item : values)
		{
			System.out.println(item);
		}
	}
	
	
	// Gibt ein 2-Dimensional Array aus
	public static void printArray(double[][] values, boolean isPi)
	{
		for (int i = 0; i < values.length; i++)
		{
			if (isPi)
			{
			System.out.print(values[i][0] * 4);
            System.out.print(" | " + values[i][1] * 4 + "\n");
			}
			else
			{
				System.out.print(values[i][0]);
	            System.out.print(" | " + values[i][1] + "\n");				
			}
		}
	}
	
	public static boolean checkRead()
	{
		double[][] data = csvHandling.readCsvFile("c:\\test.csv", ";");
		
		if (data.length > 0)
		{
			printArray(data, false);
			return true;
		}
		else
		{
			return false;
		}
	}

}
