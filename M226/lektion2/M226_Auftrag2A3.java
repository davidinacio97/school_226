package lektion2;

// Enum f�r die Speicherung der Operatoren
enum Operators
{
	Addition,
	Subtraktion,
	Multiplikation,
	Division,
	Modulo
}

public class M226_Auftrag2A3 {

	public static void main(String[] args) {
				
		System.out.println(calc(7,9, Operators.Subtraktion));
		System.out.println(calc(7,9, Operators.Addition));
		System.out.println(calc(7,9, Operators.Division));
		System.out.println(calc(7,9, Operators.Multiplikation));
		System.out.println(calc(7,9, Operators.Modulo));
	}	

	
	// Berechnet die gew�nschte Rechnung
	public static int calc(int operand1, int operand2, Operators operator)
	{
		switch(operator)
		{
			case Addition:
				return operand1 + operand2;
			case Subtraktion:
				return operand1 - operand2;
			case Multiplikation:
				return operand1 * operand2;
			case Division:
				if(operand2 == 0)
				{
					System.out.println("Divison durch 0 nicht definiert!");
					return 0;
				}
				return operand1 / operand2;
			case Modulo:
				return operand1 % operand2;
			default:
				return 0;				
		}
		
	}

}
