package lektion2;

public class M226_Auftrag2 {

	public static void main(String[] args) {
		drawStars(10);
		getZweierExponent(21);
		/*int ggt = GGT(2,3);
		System.out.println("Der GGT von 2 und 3 ist " + ggt);*/
		drawStairs(3, 10);

	}
	
	// Zeichnet ein Sterndreieck
	// Entspricht Auftrag 2, Aufgabe 1 a)
	public static void drawStars(int countLines)
	{
		String star = "*";
		for (int i = 0; i < countLines; i++)
		{
			System.out.println(star);
			star = star + "*";
		}
	}
	
	// Such die n�chst h�here 2er-Potenz der eingegebenen Zahl
	// Entspricht Auftrag 2, Aufgabe 1 b)
	public static void getZweierExponent(int zahl)
	{
		int x = 1;
		while (Math.pow(2, x) < zahl)
		{
			x++;
		}
		System.out.println("Die n�chst h�here 2er-Potenz von " + zahl + " ist " + x);
	}
	
	// Berechnet den GGT von a und b
	public static int GGT(int p, int q)
	{
		if (p  < q)
		{
			int temp = p;
			p = q;
			q = temp;			
		}
		while (q != 0)
		{
			int r = p % q;
			p = q;
			q = r;
		}

		return p;
	}
	
	// Zeichnet eine Treppe
	public static void drawStairs(int breite, int stufen)
	{		
		String stars = "";
		int tempBreite = breite;
		
		for (int i = 0; i < stufen; i++)
		{		
			stars = "";
			for (int y = 0; y < ((stufen-1) * tempBreite) - (i * tempBreite); y++)
			{
				stars +=" ";
				
			}
			for (int j = 0; j < breite; j++)
			{
				stars+= "*";
			}
			breite+=tempBreite;
			System.out.println(stars);
		}
	}
	
	
	
	

}
