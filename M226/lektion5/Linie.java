package lektion5;

import java.awt.Graphics;

/**
 * @author vmadmin
 * Linie Klasse, erbt von Figur
 */
public class Linie extends Figur {
	
	/**
	 * X-Koordinate
	 */
	private int _dX;
	
	/**
	 * Y-Koordinate 
	 */
	private int _dY;
	
	
	/**
	 * @param xPos X-Koordinate des Startpunktes
	 * @param yPos Y-Koordinate des Startpunktes
	 * @param bX X-Koordinate des Endpunktes
	 * @param bY Y-Koordinate des Endpunktes
	 * Konstruktor mir Parameter
	 */
	public Linie(int xPos, int yPos, int bX, int bY)
	{
		super(xPos, yPos);
		_dX = bX;
		_dY = bY;
	}
	
	/* (non-Javadoc)
	 * @see lektion5.Figur#move(int, int)
	 */
	public void move(int x, int y)
	{
		super.move(x, y);
		_dX += x;
		_dY += y;
	}
	
	/* (non-Javadoc)
	 * @see lektion5.Figur#zeichne(java.awt.Graphics)
	 */
	public void zeichne(Graphics g) 
	{
		g.drawLine(_posX, _posY, _dX, _dY);
	}

}
