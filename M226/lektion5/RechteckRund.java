package lektion5;

import java.awt.Graphics;


/**
 * @author vmadmin
 * RechteckRund Klasse, erbt von Rechteck
 */
public class RechteckRund extends Rechteck {
    
	/**
	 * Radius Rechteck
	 */
	private int _radius;

    /**
     * @param radius Radius
     * @param xPos X-Startpunkt
     * @param yPos Y-Startpunkt
     * @param width Breite
     * @param height Hoehe
     * Konstruktor mir Parameter
     */
    public RechteckRund(int radius, int xPos, int yPos, int width, int height) 
    {
    	super(xPos, yPos, width, height);
    	_radius = radius;
    }
    
    /* (non-Javadoc)
     * @see lektion5.Rechteck#zeichne(java.awt.Graphics)
     */
    public void zeichne(Graphics g)
    {
    	g.drawRoundRect(_posX, _posY, _width, _height, _radius, _radius);
    }

}
