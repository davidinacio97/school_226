package lektion5;

import java.awt.Graphics;

public class Rechteck extends Figur {
    protected int _width = 0;

    protected int _height = 0;

    public Rechteck(int xPos, int yPos, int width, int height) {
        super(xPos, yPos);
        _width = width;
        _height = height;
    }

    public void zeichne(Graphics g) {
        g.drawRect(_posX, _posY, _width, _height);
    }

}
