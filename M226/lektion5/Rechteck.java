package lektion5;

import java.awt.Graphics;

/**
 * @author vmadmin
 * Rechteck Klasse
 */
public class Rechteck extends Figur {
	
	/**
	 * Breite des Rechtecks
	 */
	protected int _width = 0;
	
	/**
	 * Hoehe des Rechtecks 
	 */
	protected int _height = 0;
		
	/**
	 * @param xPos X-Startpunkt
	 * @param yPos Y-Startpunkt
	 * @param width Breite
	 * @param height Hoehe
	 * Konstruktor mit Parameter
	 */
	public Rechteck(int xPos, int yPos, int width, int height)
	{
		super(xPos, yPos);
		_width = width;
		_height = height;
		
	}

	/* (non-Javadoc)
	 * @see lektion5.Figur#zeichne(java.awt.Graphics)
	 */
	public void zeichne(Graphics g)
	{
		g.drawRect(_posX, _posY, _width, _height);
		
	}
}
