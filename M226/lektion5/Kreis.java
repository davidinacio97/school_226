package lektion5;

import java.awt.Graphics;

/**
 * @author vmadmin
 * Kreis Klasse
 */
public class Kreis extends Figur {
	
	/**
	 * Breite des Kreis-Rechteckes
	 */
	private int _width = 0;
	
	/**
	 * Hoehe des Kreis-Rechteckes 
	 */
	private int _height = 0;
	
	
	/**
	 * @param posX X-Startpunkt
	 * @param posY Y-Startpunkt
	 * @param width Breite
	 * @param height Hoehe
	 * Konstruktor mit Parameter
	 */
	public Kreis(int posX, int posY, int width, int height)
	{
		super(posX, posY);
		_width = width;
		_height = height;
	}
		
	/* (non-Javadoc)
	 * @see lektion5.Figur#zeichne(java.awt.Graphics)
	 */
	public void zeichne(Graphics g)
	{
		g.drawOval(_posX, _posY, _width, _height);
	}
}
