/**
 * 
 */
package lektion5;

import java.awt.Graphics;


/**
 * @author David Dos Reis Inacio
 * Basisklasse Figur
 */
public abstract class Figur {
	
	
	/**
	 * X-Koordinate des Startpunktes. 
	 */
	protected int _posX;
		
	/**
	 * Y-Koordinate des Startpunktes. 
	 */
	protected int _posY;
	
	/**
	 * @param posX X-Startpunktkoordinate
	 * @param posY Y-Startpunktkoordinate
	 * Konstruktor mit Parameter
	 */
	public Figur(int posX, int posY)
	{
		_posX = posX;
		_posY = posY;
	}
	
	public Figur(){};
		
	/**
	 * @param posX X-Koordinatenpunkte
	 * @param posY Y-Koordinatenpunkte
	 * Move-Methode
	 */
	public void move(int posX, int posY)
	{
		_posX += posX;
		_posY += posY;
	}
	
	/**
	 * @param g Graphics Objekt
	 * Abstrakte Methode zeichne
	 */
	public abstract void zeichne(Graphics g);			
}
