package InversionOfDependency;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SchalterKlient klient = new Lampe();
		Schalter schalter = new Schalter(klient);
		
		System.out.println("Zustand Lampe: " + klient.getLeuchtStatus());
		schalter.drueckeSchalter();
		System.out.println("Zustand Lampe: " + klient.getLeuchtStatus());
		schalter.drueckeSchalter();
		System.out.println("Zustand Lampe: " + klient.getLeuchtStatus());
	}

}
