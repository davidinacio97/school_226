package InversionOfDependency;

// Klasse Schalter ist eine Klasse der h�heren Ebene
public class Schalter {
	
	private SchalterKlient _klient;
	private boolean gedrueckt;
	
	public Schalter(SchalterKlient klient)
	{
		_klient = klient;
	}
	
	public void drueckeSchalter()
	{
		gedrueckt = !gedrueckt;
		if (gedrueckt)
		{
			_klient.geheAn();
		}
		else
		{
			_klient.geheAus();
		}
	}

}
