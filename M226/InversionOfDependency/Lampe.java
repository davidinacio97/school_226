package InversionOfDependency;

public class Lampe implements SchalterKlient
{
	private boolean _leuchtet = false;
	
	public void geheAn()
	{
		_leuchtet = true;
	}
	
	public void geheAus()
	{
		_leuchtet = false;
	}
	
	public boolean getLeuchtStatus()
	{
		return _leuchtet;
	}

}
