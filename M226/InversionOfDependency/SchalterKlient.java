package InversionOfDependency;

public interface SchalterKlient 
{
	public void geheAn();
	public void geheAus();
	public boolean getLeuchtStatus();

}
