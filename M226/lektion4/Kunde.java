package lektion4;

import java.util.*;

public class Kunde {
	
	private String _name;
	private String _vorname;
	private int _kundennummer;
	private List<Konto> kontos = new ArrayList<Konto>();
	
	public Kunde(String name, String vorname, int kundennummer)
	{
		_name = name;
		_vorname = vorname;
		_kundennummer = kundennummer;
	}
	
	public void setKonto(Konto konto)
	{
		kontos.add(konto);
	}
	
	public int getKontoCount()
	{
		return kontos.size();
	}
}
