package lektion4.vererbung;

import java.util.*;

import lektion4.vererbung.Schiff.ZugelasseneGewaesser;

public class TestInherit {

	public static void main(String[] args) {
		List<Kraftfahrzeug> maschinen = new ArrayList<Kraftfahrzeug>();
		
		maschinen.add(new Auto("Mercedes-Benz", 184, 1991, 166, 450, 5, 4));
		maschinen.add(new Schiff("Queen-Marry", 5000, 50000, 8000, 60000, 2000, ZugelasseneGewaesser.AlleArtenGewaesser));

		for (Kraftfahrzeug item : maschinen)
		{
			System.out.println("Pferdestaerken: " + item.get_PferdeStaerken());
			System.out.println("Hersteller: " + item.get_Hersteller());
			System.out.println("Hubraum: " + item.get_Hubraum());
			System.out.println("H�he: " + item.get_Hoehe());
			System.out.println("Breite: " + item.get_Breite());
			System.out.println("Passagiervolumen: " + item.get_PassagierVolumen());
			System.out.println();
		}
		
		Auto car = new Auto("Audi", 190, 2000, 166, 500, 5, 4);
		Schiff ship = new Schiff("Queen-Marry", 25000, 50000, 75, 35, 1234, ZugelasseneGewaesser.Salzwasser);
		System.out.println("Zugelassene Gew�sser: " + ship.get_ZugelasseneGewaesser());
		System.out.println("Anzahl R�der: " + car.get_Raeder());
		
	}

}
