package lektion4.vererbung;

public class Schiff extends lektion4.vererbung.Kraftfahrzeug {

	enum ZugelasseneGewaesser
	{
		Salzwasser,
		Suesswasser,
		AlleArtenGewaesser,
	}
	
	ZugelasseneGewaesser _zGewaesser;
	
	
	public Schiff(String hersteller, double pferdestaerken, double hubraum, double hoehe, double breite, int passagierVolumen, ZugelasseneGewaesser gewaesser)
	{		
		super(pferdestaerken, hubraum, passagierVolumen, hoehe, breite, hersteller);
		_zGewaesser = gewaesser;
	}
	
	public ZugelasseneGewaesser get_ZugelasseneGewaesser()
	{
		return _zGewaesser;
	}
	
	public void set_ZugelasseneGewaesser(ZugelasseneGewaesser zugelasseneGewaesser)
	{
		_zGewaesser = zugelasseneGewaesser;
	}

}
