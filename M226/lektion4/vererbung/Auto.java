package lektion4.vererbung;

public class Auto extends lektion4.vererbung.Kraftfahrzeug{

	private int _raeder;
	
	public Auto(String hersteller, double pferdestaerken, double hubraum, double hoehe, double breite, int passagierVolumen, int raeder)
	{		
		super(pferdestaerken, hubraum, passagierVolumen, hoehe, breite, hersteller);
		_raeder = raeder;
	}
	
	public int get_Raeder()
	{
		return _raeder;
	}
	
	public void set_Raeder(int raeder)
	{
		_raeder = raeder;
	}
	
	
}
