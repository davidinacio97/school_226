package lektion4.vererbung;

public class Kraftfahrzeug {
	
	public double _pferdestaerken;
	public double _hubraum;
	public int _passagierVolumen;
	public double _hoehe;
	public double _breite;
	public String _hersteller;
	
	public Kraftfahrzeug(double pferdestaerken, double hubraum, int passagierVolumen , double hoehe, double breite, String hersteller)
	{
		this.set_Hersteller(hersteller);
		this.set_Breite(breite);
		this.set_Hoehe(hoehe);
		this.set_Hubraum(hubraum);
		this.set_PassagierVolumen(passagierVolumen);
		this.set_Hersteller(hersteller);
		this.set_PferdeStaerken(pferdestaerken);
	}
	
	
	public String get_Hersteller()
	{
		return _hersteller;
	}
	
	public void set_Hersteller(String hersteller)
	{
		_hersteller = hersteller;
	}
	
	
	public void set_PassagierVolumen(int passagierVolumen)
	{
		_passagierVolumen = passagierVolumen;
	}
	
	public double get_PassagierVolumen()
	{
		return _passagierVolumen;
	}
	
	
	public void set_PferdeStaerken(double pferdeStaerken)
	{
		_pferdestaerken = pferdeStaerken;
	}
	
	public double get_PferdeStaerken()
	{
		return _pferdestaerken;
	}
	
	public double get_Hubraum()
	{
		return _hubraum;
	}
	
	public void set_Hubraum(double hubraum)
	{
		_hubraum = hubraum;
	}
	
	public double get_Hoehe()
	{
		return _hoehe;
	}
	
	public void set_Hoehe(double hoehe)
	{
		_hoehe = hoehe;
	}
	
	public double get_Breite()
	{
		return _breite;
	}
	
	public void set_Breite(double breite)
	{
		_breite = breite;
	}

}
