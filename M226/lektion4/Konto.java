package lektion4;

public class Konto {

	private double _saldo;
	private double _zinssatz;
	private Kunde _inhaber;
	
	public Konto(double zinssatz, Kunde inhaber)
	{
		_zinssatz = zinssatz;
		_inhaber = inhaber;
		_inhaber.setKonto(this);
	}
	
	
	public void einzahlen(double betrag)
	{
		_saldo += betrag;
	}
	
	public void verzinsen(int tage)
	{
		_saldo += (_saldo * (_zinssatz / 100) * tage / 365);
	}
	
	public double getSaldo()
	{
		return _saldo;
	}
	
	

}
