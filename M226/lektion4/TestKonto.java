package lektion4;

public class TestKonto {

	public static void main(String[] args) {
		Kunde kunde1 = new Kunde("Duck", "Donald", 999123);
		Kunde kunde2 = new Kunde("Gates", "Bill", 666999);
		
		
		Konto k1 = new Konto(3, kunde1);
		Konto k2 = new Konto(5, kunde2);
		
		k1.einzahlen(58100);
		k2.einzahlen(108800);
		
		System.out.println("Saldo vor Zins Konto 1: " + k1.getSaldo());
		System.out.println("Saldo vor Zins Konto 2: " + k2.getSaldo());
		
		k1.verzinsen(365);
		k2.verzinsen(730);
		
		System.out.println("Saldo nach Zins Konto 1: " + k1.getSaldo());
		System.out.println("Saldo nach Zins Konto 2: " + k2.getSaldo());
		
		for (int i = 0; i < 99; i++)
		{
			Konto kx = new Konto(3.4, kunde1);
			kx.einzahlen(45000);
			kx.verzinsen(365);
		}
		
		System.out.println("Anzahl Kontos: " + kunde1.getKontoCount());

	}

}
