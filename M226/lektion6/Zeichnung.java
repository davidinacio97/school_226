package lektion6;

import java.awt.Graphics;
import java.util.*;

import lektion5.*;


/**
 * @author vmadmin
 * Verwaltet die Liste mit Figur Objekten
 */
public class Zeichnung 
{
	
	/**
	 * Liste mit Figur-Objekten 
	 */
	ArrayList<Figur> _objects;
		
	
	/**
	 * @param figurList Liste mit Figur Objekten
	 */
	public Zeichnung(ArrayList<Figur> figurList)
	{
		_objects = figurList;
	}
	
	
	/**
	 * @param g Graphics Objekt
	 * Zeichnet die einzelnen Figuren auf das Brett
	 */
	public void zeichne(Graphics g)
	{
		for (Figur f : _objects)
		{
			f.zeichne(g);			
		}
	}
	
	/**
	 * @param x X-Koordinaten Verschiebung
	 * @param y Y-Koordinaten Verschiebung
	 * Verschiebt alle Objekte um Anzahl Koordinatenpunkte
	 */
	public void move(int x, int y)
	{
		for (Figur f : _objects)
		{
			f.move(x,  y);
		}
	}
	
	
	/**
	 * @param f Figur Objekt
	 * F�gt eine neue Figur in die Liste ein.
	 */
	public void addFigur(Figur f)
	{
		_objects.add(f);
	}
}
