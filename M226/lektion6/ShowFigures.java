package lektion6;

import java.util.*;

import lektion5.*;

public class ShowFigures{

	static Zeichenbrett brett = new Zeichenbrett();
	static Zeichnung z;
	
	public static void main(String[] args) {
		
		ArrayList<Figur> figuren = new ArrayList<Figur>();
		
		// figuren.add(new RechteckRund(300, 50, 50, 300, 400));
		
		z = new Zeichnung(figuren);
		brett.zeige(z);
		
		/*
		int counter = 1;
		int radius = 5;
		
		for (int i = 0; i < 50; i++)
		{
			figuren.add(new Kreis(counter, counter, radius));
			counter++;
			radius++;
		}
		int backCounter = 150;
		int radiusforback = 5;
		
		for (int i = 0; i < 200; i++)
		{
			figuren.add(new Kreis(backCounter, backCounter, radiusforback));
			backCounter++;
			radiusforback++;
		}
*/		
	}
}
