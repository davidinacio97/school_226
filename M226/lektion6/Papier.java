package lektion6;

import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import lektion5.*;

/**
 * Papier ist eine von JPanel abgeleitete Swing-Komponente.
 * - Sie dient als "Zeichenblatt" f�r Zeichnungen.
 * - Sie speichert die Zeichnung in einer Instanzvariablen.
 * - Sie �berschreibt die Methode paintComponent der Klasse JPanel
 *   so, dass bei ihrem Aufruf die Zeichnung auf das Papier gezeichnet
 *   wird.
 * 
 * @author Andres Scheidegger
 */
@SuppressWarnings("serial")
public class Papier extends JPanel{
		
	private int x;
	private int y;
	private Zeichnung zeichnung;
	private FigurToDraw toDraw = FigurToDraw.NothingSelected;
	
	public Papier()
	{
		this.addMouseListener(new MyMouseListener());
		this.addKeyListener(new MyKeyListener());
		this.setFocusable(true);
	}
	
	
	/**
	 * Die Methode paintComponent zeichnet die Zeichnung auf das Papier.
	 * Sie erh�lt dazu ein Graphics-Objekt, welches sie an die Zeichnung
	 * weitergibt.
	 * 
	 * @param g  Graphics-Objekt, welches zum zeichnen verwendet werden soll.
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(final Graphics g) {
    super.paintComponent(g);
		if (zeichnung != null) zeichnung.zeichne(g);
	}
	
	/**
	 * Setter f�r die Instanzvariable zeichnung.
	 * 
	 * @param zeichnung Die zu zeichnende Zeichnung.
	 */
	public void setZeichnung(final Zeichnung zeichnung) {
		this.zeichnung = zeichnung;
	}	
	
	/**
	 * @param endX Endposition X-Koordinate
	 * @param endY Endposition Y-Koordinate
	 * Zeichnet eine neue Figur
	 */
	private void drawFigur(int endX, int endY)
	{
		if (toDraw != FigurToDraw.NothingSelected)
		{
			if (toDraw == FigurToDraw.Rechteck || toDraw == FigurToDraw.Kreis)
			{
				if (endX < x)
				{
					int tmp = endX;
					endX = x;
					x = tmp;
				}
				
				if (endY < y)
				{
					int tmp = endY;
					endY = y;
					y = tmp;
				}
			}
		
			switch(toDraw)
			{
			case Kreis:
				zeichnung.addFigur(new Kreis(x, y, endX - x, endY -y));
				break;
			case Rechteck:
				zeichnung.addFigur(new Rechteck(x, y, endX - x, endY - y));
				break;
			case Linie:
				zeichnung.addFigur(new Linie(x, y, endX, endY));
				break;
			case NothingSelected:
				break;
			default:
				break;				
			}
		}
		toDraw = FigurToDraw.NothingSelected;
		repaint();
	}
	
	/**
	 * @author vmadmin
	 * KeyAdapter Klasse f�r Key Listener
	 */
	private class MyKeyListener extends KeyAdapter
	{		
		public void keyPressed(KeyEvent e)
		{
			if (e.getKeyCode() == KeyEvent.VK_R)
			{
				toDraw = FigurToDraw.Rechteck;
			}
			else if (e.getKeyCode() == KeyEvent.VK_L)
			{
				toDraw = FigurToDraw.Linie;
			}
			else if (e.getKeyCode() == KeyEvent.VK_K)
			{
				toDraw = FigurToDraw.Kreis;
			}	
			else
			{
				toDraw = FigurToDraw.NothingSelected;
			}
		}
	}
	
	/**
	 * @author vmadmin
	 * MouseAdpater Klasse, f�r Mouse Listener
	 */
	private class MyMouseListener extends MouseAdapter
	{
		@Override
		public void mouseReleased(MouseEvent e) 
		{		
			drawFigur(e.getX(), e.getY());
		}
		
		@Override
		public void mousePressed(MouseEvent e) {
			x = e.getX();
			y = e.getY();		
		}
	}
}
