package party;

public class PartyCore {

	private static int nbOfGuests = 0;
	
	public static void setNbOfGuests(int countGuests)
	{
		if (countGuests > 0)
		{
			nbOfGuests = countGuests;
		}
	}
	
	public static int cheers()
	{
		return nbOfGuests * (nbOfGuests - 1) / 2;
	}

}
