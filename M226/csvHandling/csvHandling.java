package csvHandling;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class csvHandling 
{
	// Liest ein CSV File ein und parst dieses in ein double[][]
	public static double[][] readCsvFile(String csvFile, String csvSplitter)
	{
		// Reader f�r das Lesen der Datei
		BufferedReader reader = null;	
		// Zwischenspeicher f�r Line
		String line = "";
		// Zwischenspeicher f�r alle Lines
		List<String> resultList = new ArrayList<>();
				
		try 
		{
			// Reader initialisieren
			reader = new BufferedReader(new FileReader(csvFile));
			
			// Zeilenweises auslesen der Datei, f�gt die Line der Liste hinzu
			while ((line = reader.readLine()) != null)
			{
				resultList.add(line);
			}
			
			// Init des Result-Arrays
			double[][] result = new double[resultList.size()][2];
			
			// Index f�r einf�llen des Result-Arrays
			int index = 0;
			
			// Foreach Loop durch alle Strings in der String Liste
			for (String var : resultList)
			{
				try
				{
					// Versuche String zu Splitten und anschliessend in double zu verwandeln
					String[] splitted = var.split(csvSplitter);		
					result[index][0] = Double.parseDouble(splitted[0]);
					result[index][1] = Double.parseDouble(splitted[1]);
					index++;
				}
				// Falls Excepion �berspringen und weiter
				catch(NumberFormatException nfe)
				{
					continue;
				}
			}
			
			return result;
 		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			// Reader schliessen
			if (reader != null)
			{
				try
				{
					reader.close();		
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		// Falls nichts funktioniert hat, soll ein leeres double[][] zur�ckgegeben werden
		return new double[0][0];
	}
	
	// Schreibt ein CSV-File mit Daten eines double[][]
	public static void writeCsvFlie(double[][] values, String csvFile, String csvSplitter)
	{
		try
		{
			FileWriter writer = new FileWriter(csvFile);
			writer.append("Generates Result");
			writer.append(csvSplitter);
			writer.append("Difference to Math.PI");
			writer.append('\n');
			
			for (int i = 0; i < values.length; i++)
			{
				writer.append(String.valueOf(values[i][0] * 4));
				writer.append(csvSplitter);
				writer.append(String.valueOf(values[i][1]));
				writer.append('\n');
			}
			
			writer.close();
		}
		catch(IOException a)
		{
			a.printStackTrace();
		}

	}
}
