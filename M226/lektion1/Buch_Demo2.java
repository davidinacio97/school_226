package lektion1;
import java.awt.*;
import javax.swing.*;

// Author: David Dos Reis Inacio
// Date: 12.08.2014
// Class: INF2012.5K
public class Buch_Demo2 extends JFrame {

	public static void main(String[] args) {
		new Buch_Demo2();
	}
	
	public Buch_Demo2() {
		
		// Titelzeile des Programs
		super("Ein erstes Beispiel"); 
		Icon icon = new ImageIcon(getClass().getResource("duke.gif"));
				
		JLabel label = new JLabel("Viel Erfolg mit dem Grundkurs Java", icon, JLabel.CENTER);
		add(label);
		
		Font schrift = new Font("SansSerif", Font.BOLD, 20);
		label.setFont(schrift);
		label.setForeground(Color.red);
		label.setBackground(Color.white);
		label.setOpaque(true);		
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setSize(500, 200);
		
		setVisible(true);
	}

}
