package lektion1;
/**
 * 
 * @author David Dos Reis Inacio
 * 
 *
 */
public class AB01_A4 {

	public static void main(String[] args) {
	
		int a = 10;
		int b = 3;
		double c = 3.0;
		float f = 9f;
		boolean x = true;
		byte n = 3;
		
		System.out.println(a * n); // Result: 30 Typ: int UpCast von byte to int
		System.out.println(b * c); // Result: 9.0 Typ: double
		System.out.println(a / b); // Result: 3 Typ: int
		System.out.println(a / c); // Result: 3,3333 Typ: Double
		System.out.println((double) a / b); // Result: 3.33333 Typ: Double
		System.out.println((double)(a / b)); // Result: 3.0 Typ: Double
		System.out.println((int)(a / c)); // Result: 3 Typ: Int
		System.out.println(n * b == f); // Result: true Typ: boolean
		System.out.println((n << 1) + c > f); // Result: false Typ: Boolean
		System.out.println(a / c * b == f); // Result: false Typ: Boolean
		System.out.println(a / b * c == f); // Result: true Typ: Boolean
		System.out.println(a & n * n); // Result: 8 Typ: int
		System.out.println(f > 9 && !x == true); // Result: false Typ: Boolean
		System.out.println((a & n) <= c | x); // Result: true Typ: Boolean

		
		// Aufgabe 2.5 -> 3
		System.out.print("Hallo\nWellt\nJava\nist\ncool\n");
		
		// Aufgabe 2.5 -> 4
		System.out.println("Sekunden im Monat Januar " + 31 * 24 * 60* 60);
		
		// Aufgabe 2.5 -> 1
		// Hallo_Welt -> Zul�ssig
		// $Test ->  Zul�ssig
		// _abc ->  Zul�ssig
		// 2test -> nicht Zul�ssig
		// #hallo -> nicht Zul�ssig
		// te?st -> nicht Zul�ssig
		// Girokonto -> Zul�ssig
		// const -> nicht Zul�ssig
		
		
		// Aufgabe 2.5 -> 2
		// int x = 0;
		// long y = 1000;
		// x = y; --> Fehler, da long nicht in int konvertiert werden kann,
		// da long gr�sser ist 64bit zu 16bit
		
		// Aufabe 2.5 -> 5
		System.out.println("Aufgabe 2.5 -> 5: " + 5000 * 1.075);
		
		// Aufgabe 2.5 -> 6
		// a = 1
		// b = 7
		// --a -> a hat Wert 0, wird dekrementiert
		// a-- -> a hat den Wert 1 und wird dann dekrementiert
		// a++ + b -> a hat den Wert 1 und wird dann Inkrementiert, Resultat ist 8
		// b = ++a -> b hat den Wert 2
		// a = b++ -> a hat den Wert 7, b hat anschliessend Wert 8
		
		// -(a--) - -(--b) -> Resultat 5, a zum Rechnen -1 und am Schluss ist a 0
		// b zum Rechnen -6 durch 2 fache negation -1 + 6 -> 5, b am Schluss -> 6
		
		// a++ + ++a + a++ -> Resultat 7
		
	}
}
