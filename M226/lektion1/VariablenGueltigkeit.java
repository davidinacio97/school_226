package lektion1;

public class VariablenGueltigkeit {

	public static void main(String[] args) {
		int integerTest = 1;
		double doubleTest = 1.2345;
		String stringTest = "Hallo Welt";
		boolean boolTest = true;
		
		integerTest = 3;
		doubleTest = 5.535;
		stringTest = "Hallo Java";
		boolTest = false;
		
		System.out.println(integerTest);
		System.out.println(doubleTest);
		System.out.println(stringTest);
		System.out.println(boolTest);
	}

}
